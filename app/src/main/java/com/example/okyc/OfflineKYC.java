package com.example.okyc;

import android.content.Context;
import androidx.annotation.Keep;

import com.google.gson.JsonObject;

import java.io.File;

@Keep
public class OfflineKYC {

    private static OfflineKYC offlineKYC;

    private OfflineKYC() {
    }

    public static OfflineKYC getInstance() {
        if (offlineKYC == null) {
            synchronized (OfflineKYC.class) {
                if (offlineKYC == null) {
                    offlineKYC = new OfflineKYC();
                }
            }
        }

        return offlineKYC;
    }

    public void start(Context context, OfflineKYCSubmit offlineKYCSubmit, OfflineKYCResponseData responseData) {
        OKYCActivity.startOKYCActivity(context, offlineKYCSubmit, responseData);
    }

    @Keep
    public interface OfflineKYCSubmit {
        void onOKYCDownloadClick();

        void onOKYCDownloadFinish();

        void onOKYCFileAttach();

        void onOKYCShareCodeEntered();

        void onOKYCShareCodeCleared();

        void onSubmit(File file, String shareCode);

        void onError(String errorMessage);
    }

    @Keep
    public interface OfflineKYCResponseData {
        void onResponse(JsonObject jsonObject);

        void onError(String errorMessage);
    }
}
