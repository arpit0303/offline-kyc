package com.example.okyc;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.browser.customtabs.CustomTabsServiceConnection;
import androidx.browser.customtabs.CustomTabsSession;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.gson.JsonObject;

import net.lingala.zip4j.ZipFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class OKYCActivity extends Activity implements View.OnClickListener {

    private CustomTabsClient mClient;
    private CustomTabsServiceConnection mCustomTabsServiceConnection;
    private TextView tvFileName;
    private Button bSubmit;
    private EditText etShareCode;
    private ConstraintLayout clShareCode;
    private File endFile = null;

    static OfflineKYC.OfflineKYCSubmit offlineKYCSubmit;
    static OfflineKYC.OfflineKYCResponseData offlineKYCResponseData;

    private static final int STORAGE_PERMISSION_REQUEST = 100;
    private static final int FILE_REQUEST_CODE = 200;
    private static final int CUSTOM_TAB_REQUEST_CODE = 300;
    private static final String OKYC_URL = "https://myaadhaar.uidai.gov.in/";

    private long startTime = 0;

    public static void startOKYCActivity(Context context, OfflineKYC.OfflineKYCSubmit mOfflineKYCSubmit, OfflineKYC.OfflineKYCResponseData mResponseData) {
        offlineKYCSubmit = mOfflineKYCSubmit;
        offlineKYCResponseData = mResponseData;
        context.startActivity(new Intent(context, OKYCActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okyc);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
            }
        }

        tvFileName = findViewById(R.id.file_name);
        bSubmit = findViewById(R.id.submit);
        etShareCode = findViewById(R.id.sharecode);
        clShareCode = findViewById(R.id.cl_share_code);

        findViewById(R.id.download).setOnClickListener(this);
        bSubmit.setOnClickListener(this);
        findViewById(R.id.attach).setOnClickListener(this);

        etShareCode.setEnabled(false);

        etShareCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                if(s.length() == 4) {
                    offlineKYCSubmit.onOKYCShareCodeEntered();
                } else if(s.length() == 0) {
                    offlineKYCSubmit.onOKYCShareCodeCleared();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_PERMISSION_REQUEST && grantResults[0] != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_REQUEST);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.download) {
            offlineKYCSubmit.onOKYCDownloadClick();
            bSubmit.setEnabled(false);
            etShareCode.setEnabled(false);
            tvFileName.setText("");
            startTime = 0;
            mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
                @Override
                public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                    mClient = customTabsClient;
                    mClient.warmup(0L);
                    CustomTabsSession mCustomTabsSession = mClient.newSession(null);

                    CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder(mCustomTabsSession);
                    builder.enableUrlBarHiding();
                    CustomTabsIntent customTabsIntent = builder.build();
//                    customTabsIntent.launchUrl(OKYCActivity.this, Uri.parse(OKYC_URL));
                    customTabsIntent.intent.setData(Uri.parse(OKYC_URL));
                    startTime = System.currentTimeMillis();
                    startActivityForResult(customTabsIntent.intent, CUSTOM_TAB_REQUEST_CODE);
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    mClient = null;
                    startTime = 0;
                }
            };

            CustomTabsClient.bindCustomTabsService(OKYCActivity.this, "com.android.chrome", mCustomTabsServiceConnection);
        } else if (view.getId() == R.id.submit) {
            String shareCode = etShareCode.getText().toString();

            if (!shareCode.trim().isEmpty() && shareCode.length() == 4) {
//                List<File> files = Arrays.asList((new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS)).listFiles());
//
//                Iterator iterator = files.iterator();
//                File endFile = null;
//
//                while (iterator.hasNext()) {
//                    File file = (File) iterator.next();
//                    if (file.getName().startsWith("offlineaadhaar") && file.getPath().endsWith(".zip")) {
//                        if (endFile == null) {
//                            endFile = file;
//                        }
//
//                        if (endFile.lastModified() < file.lastModified()) {
//                            endFile = file;
//                        }
//                    }
//                }

                if (endFile == null) {
                    offlineKYCSubmit.onError(getResources().getString(R.string.file_not_found));
                    finish();
                    return;
                } else {
                    offlineKYCSubmit.onSubmit(endFile, shareCode);
                }

                try {
                    ZipFile zipFile = new ZipFile(endFile);
                    if (zipFile.isEncrypted()) {
                        char[] password = new char[shareCode.length()];

                        // Copy character by character into array
                        for (int i = 0; i < shareCode.length(); i++) {
                            password[i] = shareCode.charAt(i);
                        }
                        zipFile.setPassword(password);
                    }

                    String dest = Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS;
                    zipFile.extractAll(dest);

                    extractData(endFile.getPath());
                    finish();

                } catch (IOException e) {
                    Toast.makeText(OKYCActivity.this, getResources().getString(R.string.share_code_incorrect), Toast.LENGTH_SHORT).show();
                    offlineKYCResponseData.onError(getResources().getString(R.string.share_code_incorrect));
                }

            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.empty_share_code_message), Toast.LENGTH_SHORT).show();
            }
        } else if (view.getId() == R.id.attach) {
//            File file = new File(Environment.getExternalStoragePublicDirectory("") + "");
//            Uri uri = FileProvider.getUriForFile(OKYCActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
//
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setData(uri);
//            startActivityForResult(intent, FILE_REQUEST_CODE);
        }
    }

    private void extractData(String path) {
        File file = new File(path.replace(".zip", ".xml"));
        try {
            byte[] fileBytesArray = new byte[(int) file.length()];
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(fileBytesArray);

            String data = new String(fileBytesArray);
            JsonObject jsonObject = new JsonObject();

            jsonObject.addProperty("status", "success");
            jsonObject.addProperty("statusCode", "200");

            JsonObject result = new JsonObject();

            JsonObject details = new JsonObject();

            JsonObject name = new JsonObject();
            name.addProperty("value", fetchData(data, "name"));
            details.add("name", name);

            JsonObject dob = new JsonObject();
            dob.addProperty("value", fetchData(data, "dob"));
            details.add("dob", dob);

            JsonObject gender = new JsonObject();
            gender.addProperty("value", fetchData(data, "gender"));
            details.add("gender", gender);

            JsonObject address = new JsonObject();
            address.addProperty("careof", fetchData(data, "careof"));
            address.addProperty("country", fetchData(data, "country"));
            address.addProperty("house", fetchData(data, "house"));
            address.addProperty("landmark", fetchData(data, "landmark"));
            address.addProperty("pin", fetchData(data, " pc"));
            address.addProperty("state", fetchData(data, "state"));
            address.addProperty("street", fetchData(data, "street"));

            address.addProperty("locality", fetchData(data, "loc"));
            address.addProperty("postoffice", fetchData(data, "po"));
            address.addProperty("vtc", fetchData(data, "vtc"));
            address.addProperty("district", fetchData(data, "dist"));
            address.addProperty("subDistrict", fetchData(data, "subdist"));

            details.add("address", address);

            JsonObject phone = new JsonObject();
            phone.addProperty("value", fetchData(data, " m"));
            details.add("phone", phone);

            JsonObject email = new JsonObject();
            email.addProperty("value", fetchData(data, " e"));
            details.add("email", email);

            JsonObject photo = new JsonObject();
            photo.addProperty("value", fetchPhoto(data));
            details.add("photo", photo);

            details.addProperty("isVerifiedEmail", "no");
            details.addProperty("isVerifiedPhone", "no");
            details.addProperty("isVerifiedSignature", "yes");

            result.add("details", details);
            result.addProperty("type", "aadhaarXml");
            jsonObject.add("result", result);

            offlineKYCResponseData.onResponse(jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
            offlineKYCResponseData.onError(e.getMessage());
        }
    }

    private String fetchData(String data, String key) {
        String value = "";
        key = key + "=";
        if (data.contains(key)) {
            String subValue = data.substring(data.indexOf(key) + key.length() + 1);
            value = subValue.substring(0, subValue.indexOf("\""));
        }
        return value;
    }

    private String fetchPhoto(String data) {
        String value = "";
        String key = "<Pht>";
        String endKey = "</Pht>";
        if (data.contains(key)) {
            String subValue = data.substring(data.indexOf(key) + key.length());
            value = subValue.substring(0, subValue.indexOf(endKey));
        }
        return value;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CUSTOM_TAB_REQUEST_CODE) {

            offlineKYCSubmit.onOKYCDownloadFinish();

            File[] downloadFiles = new File(Environment.getExternalStorageDirectory() + File.separator + Environment.DIRECTORY_DOWNLOADS).listFiles();
            if(downloadFiles != null && downloadFiles.length > 0) {
                for (File file : downloadFiles) {
                    if (file.getName().startsWith("offlineaadhaar") && file.getPath().endsWith(".zip")) {
                        if (startTime > 0 && file.lastModified() > startTime) {
                            endFile = file;
                        }
                    }
                }

                if (endFile != null) {
                    bSubmit.setEnabled(true);
                    etShareCode.setEnabled(true);
                    tvFileName.setText("File: " + endFile.getName());
                    offlineKYCSubmit.onOKYCFileAttach();
                    clShareCode.setAlpha(1);
                } else {
                    tvFileName.setText(getResources().getString(R.string.file_not_found));
                }
            }else {
                tvFileName.setText(getResources().getString(R.string.file_not_found));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCustomTabsServiceConnection != null) {
            unbindService(mCustomTabsServiceConnection);
        }
    }
}
